# Generated by Django 3.0.3 on 2020-09-26 07:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20200926_0735'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='gid',
            field=models.CharField(blank=True, db_index=True, help_text='gateway_id', max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='payment',
            name='invoice',
            field=models.CharField(max_length=32, unique=True),
        ),
    ]
