# Generated by Django 3.0.3 on 2020-09-27 15:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_auto_20200926_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assignment',
            name='subject',
            field=models.CharField(choices=[('math', 'Mathematics'), ('stat', 'Statistics'), ('phy', 'Physics'), ('chem', 'Chemistry'), ('eco', 'Economics'), ('engg', 'Engineering'), ('mgmt', 'Management & Finance'), ('prog', 'Programming & Databases')], max_length=25),
        ),
    ]
