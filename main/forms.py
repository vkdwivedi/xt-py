from django import forms
from django.forms import ModelForm
from main.models import PriceCheck

class ContactForm(forms.Form):
    name = forms.CharField(min_length=3, max_length=25,widget=forms.TextInput(attrs={'pattern':'[A-Za-z ]*','placeholder':'John Doe','style':'margin-bottom:10px;'}),required=True)
    email = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder':'john@example.com','style':'margin-bottom:10px;'}),required=True)
    message = forms.CharField(min_length=10, max_length=500,widget=forms.Textarea(attrs={'rows':4,'placeholder':'your message goes here','style':'margin-bottom:10px;'}),required=True   )

class PriceCheckForm(ModelForm):
    class Meta:
        model = PriceCheck
        fields = ['subject','grade','duration','num_questions','short_answer','email']
    def __init__(self, *args, **kwargs):
        super(ModelForm, self).__init__(*args, **kwargs)
        self.fields['subject'].widget.attrs = {'onchange':'calculate_price()','style':'margin-bottom:10px;'}
        self.fields['grade'].widget.attrs = {'onchange':'calculate_price()','style':'margin-bottom:10px;'}
        self.fields['duration'].widget.attrs = {'onchange':'calculate_price()','style':'margin-bottom:10px;'}
        self.fields['num_questions'].widget.attrs = {'min':1,'max':999,'onchange':'calculate_price()','style':'margin-bottom:10px;'}
        self.fields['email'].widget.attrs = {'style':'margin-bottom:10px;'}
        self.fields['short_answer'].widget.attrs = {'onchange':'calculate_price()','style':'margin-bottom:10px;'}

class AssignmentUploadForm(forms.Form):
    question_text = forms.CharField(required=False,widget=forms.Textarea(attrs={'rows': 8 }))
    question_note = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 4}))
    file = forms.FileField(required=False)

class UploadAnswersForm(forms.Form):
    answer_text = forms.CharField(required=False,widget=forms.Textarea(attrs={'rows': 8 }))
    answer_note = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 4}))
    file = forms.FileField(required=False)