from django.urls import path, re_path

from . import views
from main import views

handler404 = 'main.views.errors.response_404'

urlpatterns = [
    path('', views.home.index, name='index'),
    path('about/faqs',views.about.faqs,name="about_faqs"),
    path('about/terms',views.about.terms,name="about_terms"),
    path('about/privacy',views.about.privacy,name="about_privacy"),
    path('about/contact',views.about.contact,name="about_contact"),
    path('payment/price-check',views.payment.price_check,name="price_check"),
    path('payment/confirm/<str:assignment_id>',views.payment.confirm,name="payment_confirm"),
    path('payment/add/<str:assignment_id>',views.payment.add,name="payment_add"),
    path('payment/set-price/<str:assignment_id>/<int:price>',views.payment.set,name="set_price"),
    path('payment/calculate/',views.payment.calculate,name="calculate"),
    path('assignment/<str:assignment_id>/upload/<str:cookie>',views.assignment.upload,name="assignment_upload"),
    path('assignment/uploaded/<str:assignment_id>',views.assignment.uploaded,name="assignment_uploaded"),
    path('assignment/paid_additional/<str:assignment_id>',views.assignment.paid_additional,name="paid_additional"),
    path('assignment/cancel/<str:pub_id>',views.assignment.cancel,name="cancel"),
    path('assignment/cancel_user/<str:pub_id>/<str:cookie>',views.assignment.cancel_user,name="cancel"),
    path('assignment/add_payment/<str:pub_id>',views.assignment.add_payment,name="add_payment"),
    path('assignment/upload_answers/<str:pub_id>',views.assignment.upload_answers,name="upload_answers"),
    path('assignment/refund_assign/<str:pub_id>',views.assignment.refund_assign,name="refund_assign"),
    path('stripe/webhook', views.xt_stripe.webhook, name="stripe_webhook"),
    path('stripe/create_session/<str:pub_id>',views.xt_stripe.create_session, name="create_stripe_session"),
    path('stripe/create_session_add/<str:pub_id>',views.xt_stripe.create_session_add, name="create_stripe_session"),
    path('coinbase/webhook', views.coinbase.webhook, name="coinbase_webhook"),
    path('coinbase/create_charge/<str:pub_id>', views.coinbase.create_charge, name="create_crypto"),
    path('x-bot/talk',views.x_bot.talk, name="x-bot"),
    path('help/statistics',views.subject.statistics, name="statistics"),
    path('help/programming-database',views.subject.programming, name="programming"),
    path('help/mathematics',views.subject.mathematics, name="mathematics"),
    path('help/<str:subject>',views.help.index, name="help"),
    path('assignment/review/<str:pub_id>/<str:cookie>',views.assignment.review, name="review"),
]

