from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect

from coinbase_commerce.error import WebhookInvalidPayload, SignatureVerificationError
from coinbase_commerce.webhook import Webhook
from coinbase_commerce.client import Client

import main.settings as main_settings
from django.conf import settings
from main.models import Payment, Assignment
from main.views.assignment import fulfill as assign_fulfill, refund as assign_refund



@csrf_exempt
def webhook(request):
    WEBHOOK_SECRET = 'ff09a861-c971-40cc-8183-809519e1b700'

    request_sig = request.headers.get('X-CC-Webhook-Signature', None)
    request_data = request.body.decode('utf-8')
    try:
        # signature verification and event object construction
        event = Webhook.construct_event(request_data, request_sig, WEBHOOK_SECRET)
    except (WebhookInvalidPayload, SignatureVerificationError) as e:
        return Http404(str(e))
    assignment = Assignment.objects.get(public_id=event.data['metadata']['pub_id'])
    if event.type == 'charge:confirmed':
        assign_fulfill(assignment_id=assignment.public_id, amount=assignment.price,  user_name=assignment.email, txn_details=str(event))
        payment = Payment.objects.create(invoice=assignment.public_id, amount=assignment.price, gid=event.data['code'],provider='coinbase', details=str(event))
    print("Received event: id={id}, type={type}".format(id=event.id, type=event.type))
    return HttpResponse('OK')

def create_charge(request, pub_id):
    assignment = Assignment.objects.get(public_id=pub_id)
    client = Client(api_key=main_settings.COINBASE_API_KEY)
    charge_info = {
        "name": "The Sovereign Individual",
        "description": "Mastering the Transition to the Information Age",
        "local_price": {
            "amount": str(assignment.price),
            "currency": "USD"
        },
        "metadata": {
            "pub_id": pub_id,
        },
        "pricing_type": "fixed_price",
        "redirect_url": settings.SITE_SETTINGS['url'] + "assignment/"+str(assignment.public_id)+"/upload/"+assignment.cookie,
        "cancel_url": settings.SITE_SETTINGS['url'] + "payment/confirm/" + str(assignment.public_id),
    }
    charge = client.charge.create(**charge_info)
    return redirect(charge.hosted_url)
    # Get hosted_url, id and code
