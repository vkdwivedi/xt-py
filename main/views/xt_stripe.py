import stripe, json, decimal
from django.http import HttpResponse
from django.conf import settings
from main.models import Assignment
from django.views.decorators.csrf import csrf_exempt
from main.views.assignment import fulfill as assign_fulfill, refund as assign_refund
from main.models import Payment
from datetime import datetime, timezone

@csrf_exempt
def create_session(request,pub_id):
    assignment = Assignment.objects.get(public_id=pub_id)
    stripe.api_key = settings.STRIPE_SECRET
    try:
        checkout_session = stripe.checkout.Session.create(
            client_reference_id = str(assignment.public_id),
            idempotency_key=str(assignment.public_id),
            payment_method_types=['card'],
            customer_email=assignment.email,
            line_items=[
                {
                    'price_data': {
                        'currency': 'usd',
                        'unit_amount': int(assignment.price*100),
                        'product_data': {
                            'name': 'Expert Help - XpressTutor.com',
                            'images': ['https://i.imgur.com/tTQPXsq.png'],
                        },
                    },
                    'quantity': 1,
                },
            ],
            mode='payment',
            success_url=settings.SITE_SETTINGS['url'] + "assignment/" + str(assignment.public_id) + "/upload/" + assignment.cookie,
            cancel_url=settings.SITE_SETTINGS['url'] + "payment/confirm/" + str(assignment.public_id),
        )
        return HttpResponse(json.dumps({'id': checkout_session.id}))
    except Exception as e:
        return HttpResponse(str(e))

@csrf_exempt
def create_session_add(request,pub_id):
    assignment = Assignment.objects.get(public_id=pub_id)
    stripe.api_key = settings.STRIPE_SECRET
    try:
        checkout_session = stripe.checkout.Session.create(
            client_reference_id = str(assignment.public_id)+'-ADD',
            payment_method_types=['card'],
            customer_email=assignment.email,
            line_items=[
                {
                    'price_data': {
                        'currency': 'usd',
                        'unit_amount': int((assignment.price-assignment.paid)*100),
                        'product_data': {
                            'name': 'Additional Payment for '+ str(assignment.public_id),
                            'images': ['https://i.imgur.com/tTQPXsq.png'],
                        },
                    },
                    'quantity': 1,
                },
            ],
            mode='payment',
            success_url=settings.SITE_SETTINGS['url'] + "payment/add/000" + str(assignment.public_id),
            cancel_url=settings.SITE_SETTINGS['url'] + "payment/add/" + str(assignment.public_id),
        )
        return HttpResponse(json.dumps({'id': checkout_session.id}))
    except Exception as e:
        return HttpResponse(str(e))

@csrf_exempt
def webhook(request):
    endpoint_secret = settings.STRIPE_WEBHOOK_SECRET
    stripe.api_key = settings.STRIPE_SECRET
    payload = request.body
    #print(payload)
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, endpoint_secret
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        return HttpResponse(status=400)

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        session = event['data']['object']
        amount = session["amount_total"]/100
        name = session["customer_email"].split('@')[0]
        assignment_id = session["client_reference_id"]
        # Check if there is no payment
        try:
            payment = Payment.objects.get(invoice=assignment_id)
        except Payment.DoesNotExist:
            assign_fulfill(assignment_id=assignment_id, amount=amount,  user_name=name, txn_details=str(session))
            payment = Payment.objects.create(invoice=assignment_id, amount=amount, gid=session["payment_intent"],provider='stripe', details=str(session))
    elif event['type'] == 'charge.refunded':
        session = event['data']['object']
        amount = session["amount_refunded"] / 100
        gid = session["payment_intent"]
        payment = Payment.objects.get(gid=gid)
        # Refund only once even if the webhook is called more than once
        if payment.refund == 0:
            payment.refund += decimal.Decimal(amount)
            payment.refunded_on = datetime.now(tz=timezone.utc)
            payment.save()
            assign_refund(assignment_id=payment.invoice, amount=amount)
    else:
        pass
    # Passed signature verification
    return HttpResponse(status=200)

def refund(gid,amount):
    stripe.api_key = settings.STRIPE_SECRET
    # print(amount)
    stripe.Refund.create(payment_intent=gid,amount=int(100*amount))
    return True