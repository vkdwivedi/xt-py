from django.conf import settings
from django.shortcuts import render
from django.core import mail
from django.template.loader import render_to_string
from django.http import HttpResponse
site_settings = settings.SITE_SETTINGS




def faqs(request):
    from main.utils.faq import faqs
    meta = {'title': 'Frequently Asked Questions (FAQs) | XpressTutor.com',
            'description': 'These are frequently asked questions about XpressTutor.com services. Feel free to send an email to reach out to our support page if you have any query.',
            'keywords': 'faqs, XpressTutor faqs, XpressTutor help, xpress tutor faqs'
            }
    context = {'site_settings':site_settings,'meta':meta,'faqs':faqs}
    return render(request, 'main/about/faq.html', context)
def terms(request):
    meta = {'title':'Terms of Use | XpressTutor.com',
            'keyword':'terms of use, xpresstutor terms, xpress tutor terms',
            'description':'Terms of Use for XpressTutor.com'}
    context = {'site_settings':site_settings,'meta':meta}
    return render(request, 'main/about/terms.html', context)
def privacy(request):
    meta = {'title':'Privacy Policy | XpressTutor.com',
            'keyword':'privacy policy xpresstutor, xpress tutor privacy policy',
            'description':'Privacy policy of XpressTutor.com'}
    context = {'site_settings':site_settings,'meta':meta}
    return render(request, 'main/about/privacy.html', context)
def contact(request):
    from main.forms import ContactForm
    from django.core.mail import send_mail
    from django.contrib import messages
    captcha_key = settings.NORECAPTCHA_SITE_KEY
    meta = {'title':'Contact Us | XpressTutor.com',
            'description':'You can fill in the contact form. We will get back to you in 4-8 hrs.',
            'keywords':'conatct us, contact XpressTutor, contact Xpress Tutor'
            }
    if request.method == 'POST':
        if not 'g-recaptcha-response' in request.POST:
            return HttpResponse('Bad Request')
        form = ContactForm(request.POST,label_suffix=':')
        if form.is_valid():
            name = form.cleaned_data['name']
            from_email = form.cleaned_data['email']
            message = form.cleaned_data['message']
            subject = site_settings['name']+": Contact mail from "+ name
            # Recaptcha settings
            import requests
            recaptcha_response = request.POST['g-recaptcha-response']
            data = {
                'secret': settings.NORECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
            result = r.json()
            if result['success']:
                context = {'name':name,'email':from_email,'message':message}
                msg_plain = name + '\n' + from_email + '\n' + message
                msg_html = render_to_string('main/email/contact.html', context)
                to_emails = [settings.SUPPORT_EMAIL]
                reply_tos = [from_email]
                msg = mail.EmailMessage(subject=subject,body=msg_html,from_email=from_email,to=to_emails,reply_to=reply_tos)
                msg.content_subtype = "html"
                msg.send()
                form = ContactForm(label_suffix=':')
                messages.add_message(request, messages.SUCCESS, 'Your message has been successfully sent.')
            # Save the data
            else:
                messages.add_message(request, messages.ERROR, 'Invalid reCAPTCHA! Please try again.')
    else:
        form = ContactForm(label_suffix=':')
    context = {'site_settings':site_settings,'meta':meta,'form':form,'captcha_key':captcha_key}
    return render(request, 'main/about/contact.html', context)