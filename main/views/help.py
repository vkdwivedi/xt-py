from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404


SUBJECT_MAP = {'statistics':'stat',
               'mathematics':'math',
               'programming-database':'prog',
               'engineering':'engg',
               'management-finance':'mgmt',
               'physics':'phy',
               'economics':'eco',
               'chemistry':'chem',
               'other':'stat'}
def index(request, subject):
    if subject in SUBJECT_MAP:
        sm = SUBJECT_MAP[subject]
    else:
        sm = 'stat'
    return redirect('/payment/price-check?s='+sm)