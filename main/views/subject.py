import uuid
from django.conf import settings
from django.shortcuts import render
from django.core import mail
from django.template.loader import render_to_string
from django.http import HttpResponse

site_settings = settings.SITE_SETTINGS

def statistics(request):
    meta = {'title': 'Statistics Assignment Help | R Studio, SPSS, SAS, Minitab | XpressTutor.com',
            'description': 'Get an A+ grade with top tutor help in Statistics, R, SPSS and other statistical tools for your assignment, homework and exam.',
            'keywords': 'statistics help, statistics assignment help, statistics homework help, r help, spss help, minitab help'
            }
    context = {'site_settings':site_settings,'meta':meta, 'subject':'Statistics'}
    response =  render(request, 'main/subject/statistics.html', context)
    if 'subject' not in request.COOKIES or request.COOKIES['subject'] != 'stat':
        response.set_cookie('subject','stat')
    if 'xt_user' not in request.COOKIES:
        response.set_cookie('xt_user',uuid.uuid4().hex)
    return response

def mathematics(request):
    meta = {'title': 'Mathematics Assignment Help | XpressTutor.com',
            'description': 'Get an A+ grade with top tutor help in Mathematics for your homework, assignments and exam.',
            'keywords': 'math help, math tutor help, math assignment help, math homework help, math exam help'
            }
    context = {'site_settings':site_settings,'meta':meta, 'subject':'Mathematics'}
    response = render(request, 'main/subject/math.html', context)
    if 'subject' not in request.COOKIES or request.COOKIES['subject'] != 'math':
        response.set_cookie('subject','math')
    if 'xt_user' not in request.COOKIES:
        response.set_cookie('xt_user',uuid.uuid4().hex)
    return response

def programming(request):
    meta = {'title': 'Programming Assignment Help | Python, Java, C/C++, SQL | XpressTutor.com',
            'description': 'Get an A+ grade with expert tutor help in Python, Java, C/C++ for your homework, assignment and exams.',
            'keywords': 'python homework help, python coding help, java coding help, sql help, C++ help'
            }
    context = {'site_settings':site_settings,'meta':meta, 'subject':'Programming'}
    response = render(request, 'main/subject/programming.html', context)
    if 'subject' not in request.COOKIES or request.COOKIES['subject'] != 'prog':
        response.set_cookie('subject','prog')
    if 'xt_user' not in request.COOKIES:
        response.set_cookie('xt_user',uuid.uuid4().hex)
    return response

