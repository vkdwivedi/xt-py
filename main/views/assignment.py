import decimal
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.conf import settings
from django.template.loader import render_to_string
from main.models import Assignment, Student, User, Payment, Review
import main.settings as main_settings
from datetime import datetime, timedelta, timezone
import main.utils.misc as misc_utils
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.storage import FileSystemStorage
from django.contrib import messages
from django.core.mail import send_mail, EmailMessage
from django.contrib.admin.views.decorators import staff_member_required
from main.forms import AssignmentUploadForm, UploadAnswersForm

site_settings = settings.SITE_SETTINGS

def upload(request,assignment_id,cookie):
    meta = {'title':'Upload Assignment | XpressTutor.com',
            'description':'You can upload your assignment using the text box here or mail the assignment to support@xpresstutor.com',
            'keywords':''}
    assignment = Assignment.objects.get(public_id=assignment_id)
    # See if the cookie is matching
    if (assignment.cookie != cookie):
        raise Http404('Incorrect security code')
    # Allow only one submission
    if (assignment.submitted_on is not None):
        return HttpResponseRedirect('/assignment/uploaded/'+assignment_id )
    if request.method == 'POST':
        form = AssignmentUploadForm(request.POST,request.FILES)
        if form.is_valid():
            # check the email and create a student if does not exist
            try:
                student = Student.objects.get(email = assignment.email)
            except ObjectDoesNotExist:
                student = None
            if student:
                assignment.student = student
            else:
                # Create a new user. set first 16 char of the cookie as user id
                user = User()
                user.username = misc_utils.token_alphanum(8)
                user.password = misc_utils.token_alphanum(8)
                user.email = assignment.email
                user.save()
                student = Student()
                student.email = user.email
                student.user = user
                student.save()
                assignment.student = student
            # Save the file
            if 'file' in request.FILES:
                assignment.question_file = request.FILES['file']
            dt = datetime.now()
            assignment.submitted_on = datetime.now()
            assignment.due_on = dt + timedelta(hours=assignment.duration)
            assignment.question_text = form.cleaned_data['question_text']
            assignment.question_note = form.cleaned_data['question_note']
            assignment.save()
            to_emails = [settings.SUPPORT_EMAIL]
            reply_to_emails = [assignment.email]

            email_context = {'assignment': assignment, 'site_settings': site_settings}
            subject = 'Assignment ' + str(assignment.public_id) + ' uploaded.'
            msg_html = render_to_string('main/email/uploaded-webmaster.html', email_context)
            msg = EmailMessage(subject=subject, body=msg_html, to=to_emails,reply_to=reply_to_emails)
            msg.content_subtype = 'html'
            if 'file' in request.FILES:
                msg.attach_file(assignment.question_file.file.name)
            msg.send()
            # Send mail to the user
            to_emails = [assignment.email]
            subject = site_settings['name'] + ': Assignment ' + str(assignment.public_id) + ' uploaded.'
            msg_html = render_to_string('main/email/uploaded-user.html', email_context)
            msg = EmailMessage(subject=subject, body=msg_html, to=to_emails)
            msg.content_subtype = 'html'
            if 'file' in request.FILES:
                msg.attach_file(assignment.question_file.file.name)
            msg.send()

            success_message = 'Your question for this assignment [' + str(assignment.public_id) + '] is successfully uploaded. Please send a mail to ' + site_settings["support"] + ' if you need help.'
            if assignment.num_questions > 1:
                success_message = 'Your questions for this assignment [' + str(assignment.public_id) + '] are successfully uploaded. Please send a mail to ' + site_settings["support"] + ' if you need help.'
            messages.add_message(request, messages.SUCCESS, success_message)
            return HttpResponseRedirect('/assignment/uploaded/'+assignment_id)
    else:
        form = AssignmentUploadForm()
        context = {'site_settings':site_settings,'meta':meta,'form':form, 'assignment':assignment}
        response = render(request, 'main/assignment/upload-questions.html', context)
        return response


# Payment confirmation has come
def fulfill(assignment_id,amount, user_name, txn_details):
    if 'ADD' in assignment_id:
        assignment_id = assignment_id[:-4]
        payment_type = "add"
        assignment = Assignment.objects.get(public_id=assignment_id)
        assignment.status = 'PAID'
        assignment.additional_payment = amount
        assignment.additional_payment_on = datetime.now(timezone.utc)
        assignment.save()
        subject = "Additional Payment of $" + str(int(amount)) + " received for " + str(assignment.public_id)
    else:
        payment_type = "new"
        assignment = Assignment.objects.get(public_id=assignment_id)
        assignment.status = 'PAID'
        assignment.paid = amount
        assignment.paid_on = datetime.now(timezone.utc)
        assignment.save()
        subject = "Payment of $" + str(int(amount)) + " received for " + str(assignment.public_id)

    # Now send a mail to the user
    to_emails = [assignment.email]
    context = {'site_settings': settings.SITE_SETTINGS,'assignment': assignment, 'is_additional':payment_type != "new", 'name':user_name}
    msg_html = render_to_string('main/email/payment-confirmation-user.html', context)
    msg = EmailMessage(subject=subject, body=msg_html, to=to_emails)
    msg.content_subtype = 'html'
    msg.send()

    # Send mail to webmaster
    to_emails = [settings.SUPPORT_EMAIL]
    context["txn_details"] = txn_details
    msg_html = render_to_string('main/email/payment-confirmation-webmaster.html', context)
    msg = EmailMessage(subject=subject, body=msg_html, to=to_emails)
    msg.content_subtype = 'html'
    msg.send()

def refund(assignment_id, amount):
    refund_type = "main"
    if 'ADD' in assignment_id:
        assignment_id = assignment_id[:-4]
        refund_type = "additional"
    assignment = Assignment.objects.get(public_id=assignment_id)
    if refund_type == "main" and assignment.refund == 0:
        assignment.refund -= decimal.Decimal(amount)
    elif assignment.refund_additional == 0:
        assignment.refund_additional -= decimal.Decimal(amount)
    assignment.save()

def uploaded(request,assignment_id):
    meta = {'title': 'Uploaded',
            'description': '',
            'keywords': ''}
    data = {'h1':'ID '+assignment_id,
            'h2':'submitted successfully',
            'p1':'Your assignment is submitted successfully. We will email you the answers within given timeline. Please send an mail to <span class="text-white">support@xpresstutor.com</span> or reach out to our <span class="text-white">chat support</span>,  if you have any clarification.',
            'btn_url':'/payment/price-check',
            'btn_text':'Submit Another'}
    context = {'meta':meta, 'data':data}
    return render(request, 'main/message.html', context)

def paid_additional(request,assignment_id):
    meta = {'title': 'Uploaded',
            'description': '',
            'keywords': ''}
    data = {'h1':'Payment Successful!',
            'h2':'ID: %s-ADD' % (assignment_id),
            'p1':'Your payment for %s is made successfully. Please send an mail to <span class="text-white">support@xpresstutor.com</span> or reach out to our <span class="text-white">chat support</span>,  if you have any clarification.' % (assignment_id),
            'btn_url':'/payment/price-check',
            'btn_text':'Submit Another'}
    context = {'meta':meta, 'data':data}
    return render(request, 'main/message.html', context)

def initiate_refund(assign):
    refund_amount = assign.paid + assign.additional_payment + assign.refund + assign.refund_additional - assign.price
    # Do not refund completed assignments
    if refund_amount <= 0 or assign.status == 'COMPLETED':
        return False
    from main.views.xt_stripe import refund as stripe_refund

    # Refund is possible only once
    main_refund = refund_amount
    add_refund = 0
    # First refund from additional payment
    if assign.additional_payment > 0:
        if refund_amount > assign.additional_payment:
            add_refund = assign.additional_payment
            main_refund -= add_refund
        else:
            add_refund = refund_amount
            main_refund = 0
    if main_refund > 0:
        pmt = Payment.objects.get(invoice = str(assign.public_id))
        # Refund only once
        if pmt.refund == 0:
            if pmt.provider == 'stripe':
                stripe_refund(pmt.gid,main_refund)
        else:
            return False
    if add_refund > 0:
        add_pmt = Payment.objects.get(invoice = str(assign.public_id)+'-ADD')
        if add_pmt.refund == 0:
            if add_pmt.provider == 'stripe':
                stripe_refund(add_pmt.gid,add_refund)
        else:
            return False
    return True

@staff_member_required
def refund_assign(request,pub_id):
    assign = Assignment.objects.get(public_id=pub_id)
    val = initiate_refund(assign)
    if val:
        messages.add_message(request, messages.SUCCESS, 'Assignment %s is refunded' % (pub_id,))
    else:
        messages.add_message(request, messages.ERROR, 'Could not refund')
    return HttpResponse('Done')

@staff_member_required
def cancel(request, pub_id):
    assignment = Assignment.objects.get(public_id=pub_id)
    assignment.status = 'CANCELLED'
    assignment.price = 0
    assignment.save()
    # Now send a mail to the user
    email_context = {'assignment': assignment, 'site_settings': settings.SITE_SETTINGS, 'name':assignment.email.split('@')[0], 'user_cancelled':False}
    to_emails = [assignment.email]
    subject = 'Your assignment %s is cancelled' % (pub_id,)
    msg_html = render_to_string('main/email/cancelled-user.html', email_context)
    msg = EmailMessage(subject=subject, body=msg_html, to=to_emails)
    msg.content_subtype = 'html'
    msg.send()
    messages.add_message(request,messages.SUCCESS,'Assignment %s is cancelled. An email is sent to the user.' % (pub_id,))
    return HttpResponse('Done')

def cancel_user(request, pub_id, cookie):
    assignment = Assignment.objects.get(public_id=pub_id)
    if assignment.cookie != cookie or assignment.status != 'ADD_PAY':
        raise Http404('No such assignment')
    assignment.status = 'CANCELLED'
    assignment.cancellation_reason = 'USER_CAN'
    assignment.price = 0
    assignment.save()
    # Now send a mail to the user
    email_context = {'assignment': assignment, 'site_settings': settings.SITE_SETTINGS, 'name':assignment.email.split('@')[0],'user_cancelled':True}
    to_emails = [assignment.email]
    cc_emails = [settings.SUPPORT_EMAIL]
    subject = 'Your assignment %s is cancelled' % (pub_id,)
    msg_html = render_to_string('main/email/cancelled-user.html', email_context)
    msg = EmailMessage(subject=subject, body=msg_html, to=to_emails, cc=cc_emails)
    msg.content_subtype = 'html'
    msg.send()
    messages.add_message(request, messages.SUCCESS, 'Your assignment %s is cancelled. The money will be refunded to your payment method in 6-8 working days' % (pub_id,))
    return redirect('/payment/price-check')

@staff_member_required
def add_payment(request, pub_id):
    assignment = Assignment.objects.get(public_id=pub_id)
    assignment.status = 'ADD_PAY'
    assignment.save()
    add_pay = assignment.price - assignment.paid

    # Now send a mail to the user
    email_context = {'assignment': assignment, 'add_pay': add_pay,'reason':main_settings.ADD_PAY_REASONS_DES[assignment.add_payment_reason], 'site_settings': settings.SITE_SETTINGS, 'name':assignment.email.split('@')[0]}
    to_emails = [assignment.email]
    subject = 'Additional payment request of $'+ str(add_pay)
    msg_html = render_to_string('main/email/request_add_payment.html', email_context)
    msg = EmailMessage(subject=subject, body=msg_html, to=to_emails)
    msg.content_subtype = 'html'
    msg.send()

    messages.add_message(request, messages.SUCCESS, 'Email is sent for an additional payment of $%s' % (add_pay,))
    return HttpResponse('Done')

@staff_member_required
def upload_answers(request, pub_id):
    meta = {'title': 'Upload Answers for %s | XpressTutor.com' % (pub_id,),
            'description': 'Upload answers for the assignment',
            'keywords': ''}
    assignment = Assignment.objects.get(public_id=pub_id)
    if assignment.status in ['COMPLETED', 'CANCELLED']:
        raise Http404('Status is completed/cancelled')
    if request.method == 'POST':
        form = UploadAnswersForm(request.POST,request.FILES)
        if form.is_valid():
            # Save the file
            if 'file' in request.FILES:
                assignment.answer_file = request.FILES['file']
            assignment.answered_on = datetime.now()
            assignment.answer_text = form.cleaned_data['answer_text']
            assignment.question_note = form.cleaned_data['answer_note']
            assignment.status = 'COMPLETED'
            assignment.save()

            # Send mail to the user
            email_context = {'assignment': assignment, 'site_settings': site_settings}
            to_emails = [assignment.email]
            subject = site_settings['name'] + ': Your answers for assignment ' + str(assignment.public_id) + ' are here.'
            msg_html = render_to_string('main/email/answered-user.html', email_context)
            msg = EmailMessage(subject=subject, body=msg_html, to=to_emails)
            msg.content_subtype = 'html'
            if 'file' in request.FILES:
                msg.attach_file(assignment.answer_file.file.name)
            msg.send()
            messages.add_message(request, messages.SUCCESS, 'The answers for %s are uploaded successfully ' % (pub_id,))
            return redirect('/admin/main/dashboard/')
    else:
        form = UploadAnswersForm()
        context = {'site_settings':site_settings,'meta':meta,'form':form, 'assignment':assignment}
        response = render(request, 'main/assignment/upload-answers.html', context)
        return response

def review(request,pub_id,cookie):
    assignment = Assignment.objects.get(public_id=pub_id)
    if assignment.cookie != cookie:
        raise Http404('Secret key does not match')
    if assignment.status != 'COMPLETED':
        raise Http404('Assignment is not closed yet.')
    if request.method == 'POST':
        # Create a review object
        rating = int(request.POST['score'])
        review_text = request.POST['review_text']
        review = Review.objects.create(rating=rating, review_txt=review_text, assign=assignment)
        messages.add_message(request,messages.SUCCESS, 'Thanks for submitting your review')
        return redirect('/payment/price-check')
        # Take them to thank you page
    meta = {'title': 'Review your tutor | XpressTutor.com',
            'description': 'Rate and review your tutor for your assignment.',
            'keywords': 'assignment help, homework help'
            }
    context = {'site_settings':site_settings,'meta':meta,'assign':assignment}
    return render(request, 'main/assignment/review.html', context)