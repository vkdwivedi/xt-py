import stripe, json, uuid
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from paypal.standard.forms import PayPalPaymentsForm
from paypal.standard.forms import PayPalEncryptedPaymentsForm
from django.conf import settings
from main.models import Assignment, PriceCheck
import main.settings as main_settings
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from paypal.standard.ipn.models import PayPalIPN
from django.urls import reverse
from django.contrib import messages
from django.http import Http404
from decimal import Decimal


# Create your views here.

def price_check(request,subject='all'):
    from main.forms import PriceCheckForm
    site_settings = settings.SITE_SETTINGS
    crypto_enabled = False
    default_subject = 'stat'
    if 'subject' in request.COOKIES:
        default_subject = request.COOKIES.get('subject')
    if 's' in request.GET:
        default_subject = request.GET['s']
    meta = {'title':'Calculate Price | Get Quick Quotation for your Assignment',
            'description':'Calculate price and get quick quotation for your assignment',
            'keywords':'price calculator, get quotation, assignment help'}
    if request.method == 'POST':
        form = PriceCheckForm(request.POST)
        if form.is_valid():
            assignment = Assignment()
            assignment.subject = form.cleaned_data['subject']
            assignment.grade = form.cleaned_data['grade']
            assignment.duration = int(form.cleaned_data['duration'])
            assignment.num_questions =int(form.cleaned_data['num_questions'])
            assignment.short_answer = form.cleaned_data['short_answer']
            dt = datetime.now()
            day_id = (dt.year % 100)*10000 + dt.month * 100 + dt.day
            day_id = day_id * 10000
            num_assign_today = Assignment.objects.filter(public_id__gt = day_id).count()
            pub_id = num_assign_today + 1
            assignment.public_id = day_id + pub_id
            assignment.year_month = (dt.year % 100)*100 + dt.month
            assignment.email = form.cleaned_data['email']
            pc = PriceCheck(subject=assignment.subject,grade=assignment.grade,duration=assignment.duration,num_questions=assignment.num_questions,short_answer=assignment.short_answer)
            assignment.price = pc.calculate_price()
            assignment.cookie = request.COOKIES['xt_user']
            # Get IP address of the client
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                ipaddress = x_forwarded_for.split(',')[-1].strip()
            else:
                ipaddress = request.META.get('REMOTE_ADDR')
            assignment.IP = ipaddress
            assignment.save()
            response = HttpResponseRedirect('/payment/confirm/'+str(assignment.public_id))
            response.set_cookie('email',form.cleaned_data['email'],max_age=2592000)
            return response
    else:
        default = {'subject':default_subject}
        if 'email' in request.COOKIES:
            default['email'] = request.COOKIES['email']
        if 'g' in request.GET:
            default['subject'] = request.GET['s']
            default['grade'] = request.GET['g']
            default['duration'] = request.GET['d']
            default['num_questions'] = request.GET['n']
            if request.GET['a'] == '1':
                default['short_answer'] = True
            else:
                default['short_answer'] = False
        if 'crypto' in request.GET:
            crypto_enabled = True
        form = PriceCheckForm(initial=default)
    context = {'site_settings':site_settings,'meta':meta,'form':form}
    response = render(request, 'main/payment/price-check.html', context)
    if 'xt_user' not in request.COOKIES:
        response.set_cookie('xt_user', uuid.uuid4().hex, max_age=2592000)
    if crypto_enabled:
        response.set_cookie('crypto', 'yes', max_age=2592000)
    if 'subject' not in request.COOKIES or default_subject != request.COOKIES['subject']:
        response.set_cookie('subject',default_subject)

    return response


def confirm(request,assignment_id):
    from main.utils.faq import faqs
    assignment = Assignment.objects.get(public_id=assignment_id)
    crypto_enabled = False
    if assignment.short_answer:
        steps = ' Only Answer(s)'
    else:
        steps = ' All the Steps'
    assign_json = {'price':int(assignment.price),'subject':main_settings.SUBJECTS[assignment.subject]['name'],'number':assignment.num_questions,'duration':assignment.duration,'grade':main_settings.GRADES[assignment.grade]['name'],'steps':steps}
    ipns = PayPalIPN.objects.filter(custom=assignment.email)
    cookie = assignment.cookie
    # TODO check previous assignemnts with payment success and same cookie
    #previous_assignemnts = Assignment.objects.filter(cookie=cookie)
    site_settings = settings.SITE_SETTINGS
    meta = {'title':'Price Confirmation | XpressTutor.com',
            'description': 'Verify details and confirm the price',
            'keywords': 'assignment help, xpress tutor help',
              }
    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "currency_code":"USD",
        "amount": str(int(assignment.price*Decimal(1.02))),
        "item_name": "Expert Help",
        "item_number":"1",
        "invoice": str(assignment.public_id),
        "no_shipping": "1",
        "notify_url": settings.PAYPAL_SITE_URL + reverse('paypal-ipn'),
        "cancel_return": settings.SITE_SETTINGS['url'] + "payment/confirm/"+str(assignment.public_id),
        "return_url": settings.SITE_SETTINGS['url'] + "assignment/"+str(assignment.public_id)+"/upload/"+cookie,
        "custom": assignment.email,  # Custom will store student's email id
    }
    # If the user exists, add address fields automatically
    if len(ipns) > 0:
        paypal_dict["first_name"] = ipns[0].first_name
        paypal_dict["last_name"] = ipns[0].last_name
        paypal_dict["address_name"] = ipns[0].address_name
        paypal_dict["address_street"] = ipns[0].address_street
        paypal_dict["address_city"] = ipns[0].address_city
        paypal_dict["address_state"] = ipns[0].address_state
        paypal_dict["address_country_code"] = ipns[0].address_country_code
        paypal_dict["address_zip"] = ipns[0].address_zip
    # Create the instance.
    if settings.HOST_ENV == 'local':
        form = PayPalPaymentsForm(initial=paypal_dict)
    else:
        form = PayPalEncryptedPaymentsForm(initial=paypal_dict)
    form_text = form.render()
    form_text = form_text.replace('<form ','<form id="paypal-button" ')
    # Clean some junk char coming
    form_text = form_text.replace('\\n', '')
    form_text = form_text.replace("b'-", "-")
    form_text = form_text.replace("-'", "-")
    if settings.HOST_ENV == 'server':
        btn_old = '<input type="image" src="https://images.paypal.com/images/x-click-but01.gif" border="0" name="submit" alt="Buy it Now" />'
    else:
        btn_old = '<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="Buy it Now" />'
    btn_new = '<button type="submit" class="btn btn-success blt-lg" border="0" name="submit" alt="Make Payments with PayPal!"><i class="fa fa-credit-card"></i> Pay Now</button>'
    form_text = form_text.replace(btn_old,btn_new)
    if 'crypto' in request.COOKIES:
        crypto_enabled = settings.CRYPTO_ENABLED and (request.COOKIES['crypto']=='yes')
    context = {'site_settings': site_settings, 'meta': meta, 'paypal_form': form_text, 'assign':assign_json, 'settings':settings, 'assignment':assignment, 'crypto_enabled':crypto_enabled,'faqs':faqs}
    return render(request, "main/payment/price-confirm.html", context)

def add(request,assignment_id):
    from main.utils.faq import faqs
    if assignment_id[:3] == "000":
        # The payment is successful
        assignment_id = assignment_id[3:]
        return HttpResponseRedirect('/assignment/paid_additional/'+assignment_id)

    assignment = get_object_or_404(Assignment, public_id=assignment_id)
    if assignment.additional_payment_on is not None or assignment.status in ['CANCELLED', 'COMPLETED']:
        msg = "Nothing pending for assignment <b>"+assignment_id+"</b>. Please get in touch with support if you need help."
        messages.add_message(request, messages.ERROR, msg)
        return HttpResponseRedirect('/payment/price-check')
    if assignment.short_answer:
        steps = ' Only Answer(s)'
    else:
        steps = ' All the Steps'
    price = int(assignment.price - assignment.paid)
    ipns = PayPalIPN.objects.filter(custom=assignment.email)
    site_settings = settings.SITE_SETTINGS
    meta = {'title':'Make Additional Payment | XpressTutor.com',
              'description':'Verify details and make additional payment for your assignmnent '+assignment_id,
              'keywords':'add payment, payment tutor',
              }
    paypal_dict = {
        "business": settings.PAYPAL_RECEIVER_EMAIL,
        "currency_code":"USD",
        "amount": str(price*1.02),
        "item_name": "Expert Help",
        "item_number":"1",
        "invoice": str(assignment.public_id)+'-ADD',
        "no_shipping": "1",
        "notify_url": settings.PAYPAL_SITE_URL + reverse('paypal-ipn'),
        "cancel_return": settings.SITE_SETTINGS['url'] + "payment/add/"+str(assignment.public_id),
        "return_url": settings.SITE_SETTINGS['url'] + "payment/add/000"+str(assignment.public_id),
        "custom": assignment.email,  # Custom will store student's email id
    }
    # If the user exists, add address fields automatically
    if len(ipns) > 0:
        paypal_dict["first_name"] = ipns[0].first_name
        paypal_dict["last_name"] = ipns[0].last_name
        paypal_dict["address_name"] = ipns[0].address_name
        paypal_dict["address_street"] = ipns[0].address_street
        paypal_dict["address_city"] = ipns[0].address_city
        paypal_dict["address_state"] = ipns[0].address_state
        paypal_dict["address_country_code"] = ipns[0].address_country_code
        paypal_dict["address_zip"] = ipns[0].address_zip
    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    #form = PayPalEncryptedPaymentsForm(initial=paypal_dict)
    form_text = form.render()
    form_text = form_text.replace('<form ','<form id="paypal-button" ')
    if settings.HOST_ENV == 'server':
        btn_old = '<input type="image" src="https://images.paypal.com/images/x-click-but01.gif" border="0" name="submit" alt="Buy it Now">'
    else:
        btn_old = '<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="Buy it Now" />'
    btn_new = '<button type="submit" class="btn btn-success blt-lg" border="0" name="submit" alt="Make Payments with PayPal!"><i class="fa fa-credit-card"></i> Pay Now</button>'
    form_text = form_text.replace(btn_old,btn_new)
    add_faqs = faqs['price_add']
    add_faqs[1]['answer'] = add_faqs[1]['answer'] % (assignment.public_id,assignment.cookie)
    context = {'site_settings': site_settings, 'meta': meta, 'paypal_form': form_text, 'price':price, 'settings':settings, 'assignment':assignment,'faqs':add_faqs}
    return render(request, "main/payment/price-add.html", context)

def set(request,assignment_id,price):
    assignment = get_object_or_404(Assignment,public_id=assignment_id)
    assignment.price = price
    assignment.save()
    msg = "Price of " + assignment_id + " is now set to $" + price
    messages.add_message(request, messages.SUCCESS, msg)
    return HttpResponseRedirect('/')

@csrf_exempt
def calculate(request):
    data = request.POST
    pc = PriceCheck()
    try:
        pc.cookie = request.COOKIES.get('xt_user')
        pc.subject = data['subject']
        pc.duration = int(data['duration'])
        pc.grade = data['grade']
        pc.num_questions = int(data['num_questions'])
        pc.short_answer = True
        if data['short_answer'] == 'false':
            pc.short_answer = False
    except:
        raise Http404("POST data not present")
    pc.price = pc.calculate_price()
    pc.save()
    response = HttpResponse(pc.price)
    return response

