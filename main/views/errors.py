from django.shortcuts import render
from django.conf import settings

# HTTP Error 404
def response_404(request,exception):
    site_settings = settings.SITE_SETTINGS
    meta = {'title':'Contact Us',
            'keywords':'contact '+ site_settings['name'],
            'description':'You can use this page to contact '+site_settings['name']}
    context = {'site_settings': site_settings, 'meta': meta}
    response = render(request,'main/errors/404.html',context)
    response.status_code = 404
    return response