import json
from datetime import timedelta, timezone, datetime
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
import main.settings as main_settings

from main.models import Assignment, PriceCheck

@csrf_exempt
def talk(request):
    body_json = json.loads(request.body)
    intent_name = body_json['queryResult']['intent']['displayName']
    question = body_json['queryResult']['queryText']
    fulfilled = True
    resp = {
        "fulfillmentMessages": []
    }
    if intent_name in ['status','cancel','refund']:
        assignment_id = body_json['queryResult']['parameters']['assignment_id']
        msg = ass_stat(pub_id=assignment_id)
        resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
        if intent_name  in ['cancel','refund']:
            msg = 'Please send request e-mail to support@xpresstutor.com with your assignment ID in subject'
            resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
    elif intent_name == 'help':
        subject =  body_json['queryResult']['parameters']['subject']
        num_questions =  int(body_json['queryResult']['parameters']['num_questions'])
        email = body_json['queryResult']['parameters']['email']
        dur_json = body_json['queryResult']['parameters']['duration']

        if 'date_time' in dur_json:
            diff = datetime.fromisoformat(dur_json['date_time']) - datetime.now(timezone.utc)
        elif 'startDateTime' in dur_json:
            diff = datetime.fromisoformat(dur_json['endDateTime']) - datetime.fromisoformat(dur_json['startDateTime'])
        else:
            diff = datetime.fromisoformat(dur_json) - datetime.now(timezone.utc)
        dur_hr = diff.days * 24 + diff.seconds/3600
        if dur_hr < 6:
            msg = 'We have an option of live help/tutoring but you need to book in advance for that.'
            resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
            dur_hr = 6
        if dur_hr > 6:
            dur_hr = 6 * (dur_hr//6)
        if dur_hr > 12:
            dur_hr = 12 * (dur_hr//12)
        if dur_hr > 24:
            dur_hr = 24 * (dur_hr//24)
        if dur_hr > 72:
            dur_hr = 72
        dur_hr = int(dur_hr)
        pc = PriceCheck()
        pc.subject = subject
        pc.duration = dur_hr
        pc.num_questions = num_questions
        pc.email = email
        pc.short_answer = True
        price = pc.calculate_price()
        pc.price = price
        pc.save()
        msg = "Total price for this assignment for answers/ multiple choices will be **$%s**" % (price,)
        resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
        msg = "If you need detailed working/steps, the price will be little higher."
        resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
        msg = "Please send your questions to **suport@xpresstutor.com** for exact quote."
        resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
        msg = "You can use our price-calculator to estimate price for different subjects/grades by clicking [here](https://www.xpresstutor.com/payment/price-check?s=%s&g=college&d=%s&n=%s&a=0)." % (subject,dur_hr,num_questions)
        resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
    elif intent_name == 'human':
        msg = 'Our live agents are available in standard office hours at Perth/Australia and London/United Kingdom timezones.'
        resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
        nw = datetime.now(timezone.utc)
        diff = 25 - nw.hour
        mn = nw.minute
        if diff < 8:
            msg = 'Next live agent will be available in %s hrs and %s minutes from now' % (diff,60-mn)
            resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
        msg = 'You can send an email to support@xpresstutor.com for a quick quote on your assignment/questions.'
        resp['fulfillmentMessages'].append({"text":{"text":[msg]}})
    return HttpResponse(json.dumps(resp))

def ass_stat(pub_id):
    assigns = Assignment.objects.filter(public_id=pub_id)
    if len(assigns) > 0:
        assign = assigns[0]
    else:
        return 'There is no assignment with this id'
    if assign.status == 'CANCELLED':
        if assign.refunded():
            resp = 'Your assignment is cancelled and refunded.'
        else:
            resp = 'Your assignment is cancelled. The refund will be processed in few days. Payments processed through Paypal may take a little longer to refund'
    elif assign.status == 'PAID':
        if assign.due_on is not None:
            diff = assign.due_on - datetime.now(timezone.utc)
            if diff > timedelta(0):
                if diff.days > 0:
                    resp = 'Your assignment is in process and will be given in %s day/s and %s hours' % (diff.days,diff.seconds//3600)
                else:
                    resp = 'Your assignment is in process and will be given in %s hours and %s minutes' % (diff.seconds//3600, (diff.seconds//60)%60)
            else:
                resp = 'I see that it is in progress. Can you send an e-mail to support@xpresstutor.com to check the scheduled delivery time?.'
    elif assign.status == 'COMPLETED':
        resp = 'Your assignment is completed. The answers have been emailed to you.'
    else:
        resp = 'Please send a mail to support@xpresstutor.com for status.'
    return resp