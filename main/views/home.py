from django.shortcuts import render
from django.conf import settings
import uuid

# Create your views here.

def index(request):
    site_settings = settings.SITE_SETTINGS
    meta = {
        'title':'Score A+ with expert assignment and homework help in Statistics, Mathematics, Programming, Finance, Physics, Economics, Engineering and other subjects',
        'description': site_settings['name'] + ' provides expert homework and assignment help in 100+ subjects including Statistics, Mathematics, Programming, Finance, Physics, Economics, Engineering. Top quality tutors from premier colleges are avialble 24/7.',
        'keywords': 'expert help,homework help, assignment help, math help, statistics help, statistics tutor, math tutor',
        }
    context = {'site_settings':site_settings,'meta':meta}
    response = render(request, 'main/home.html', context)
    if 's' in request.GET:
        response.set_cookie('subject',request.GET['s'])
    if 'xt_user' not in request.COOKIES:
        response.set_cookie('xt_user',uuid.uuid4().hex)
    return response