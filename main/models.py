from django.db import models
import main.utils as main_utils
from django.contrib.auth.models import User
from django.utils.html import format_html
from datetime import datetime, timezone
import main.settings as main_settings


# Create your models here.
class Address(models.Model):
    address1 = models.CharField(max_length=50)
    address2 = models.CharField(max_length=50)
    city = models.CharField(max_length=25)
    zip_code = models.CharField(max_length=8)
    country_code = models.CharField(max_length=2)
    created_on = models.DateTimeField(auto_now_add=True,editable=False)

class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField(unique=True,verbose_name="Email")
    address = models.ForeignKey(Address,on_delete=models.CASCADE,null=True)
    phone = models.CharField(max_length=12,null=True)
    timezone = models.CharField(max_length=25,null=True,blank=True)
    created_on = models.DateTimeField(auto_now_add=True,editable=False)
    def __str__(self):
        return self.email

class Tutor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    email = models.EmailField(null=True,unique=True,verbose_name="Email",blank=True)
    address = models.ForeignKey(Address,on_delete=models.CASCADE,null=True,blank=True)
    phone = models.CharField(max_length=12,null=True,blank=True)
    pan = models.CharField(max_length=10,null=True,blank=True)
    bank_name = models.CharField(null=True,max_length=50,blank=True)
    account_no = models.CharField(max_length=25,null=True,blank=True)
    ifsc_code = models.CharField(max_length=12,null=True,blank=True)
    is_active = models.BooleanField(default=True,blank=True)
    created_on = models.DateTimeField(auto_now_add=True,editable=False)
    def __str__(self):
        return self.name
def assignment_path(instance, filename):
    return 'assignments/{0}/{1}'.format(instance.public_id, filename)

class Assignment(models.Model):
    public_id = models.BigIntegerField(unique=True)
    year_month = models.IntegerField(default=0)
    email = models.EmailField(verbose_name="Email")
    student = models.ForeignKey(Student,null=True, on_delete=models.PROTECT)
    assigned_to = models.ForeignKey(Tutor,null=True,blank=True,on_delete=models.PROTECT)
    subject = models.CharField(max_length=25,choices=main_settings.SUBJECT_CHOICES)
    grade = models.CharField(max_length=25, choices=main_settings.GRADE_CHOICES,)
    duration = models.IntegerField()
    num_questions = models.IntegerField()
    short_answer = models.BooleanField()
    price = models.DecimalField(max_digits=8,decimal_places=2)
    tutor_payment = models.DecimalField(max_digits=8,decimal_places=2,default=0)
    paid = models.DecimalField(default=0,max_digits=8,decimal_places=2)
    paid_on = models.DateTimeField(null=True,blank=True)
    refund = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    refund_additional = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    additional_payment = models.DecimalField(default=0,max_digits=8,decimal_places=2)
    add_payment_reason = models.CharField(null=True,blank=True, max_length=64, choices=main_settings.ADD_PAY_REASONS)
    add_payment_note = models.CharField(null=True, blank=True, max_length=512,help_text='Make sure that this note is saved before requesting payment.')
    additional_payment_on = models.DateTimeField(null=True,blank=True)
    currency = models.CharField(max_length=4,default='USD')
    created_on = models.DateTimeField(auto_now_add=True,editable=False)
    submitted_on = models.DateTimeField(null=True)
    answered_on = models.DateTimeField(null=True)
    due_on = models.DateTimeField(null=True)
    solution_on = models.DateTimeField(null=True,blank=True)
    status = models.CharField(default='NEW', max_length=32, choices=main_settings.ASSIGNMENT_STATUSES,)
    question_text = models.TextField(null=True, blank=True)
    answer_text = models.TextField(null=True, blank=True)
    question_note = models.TextField(null=True,blank=True)
    answer_note = models.TextField(null=True,blank=True)
    question_file = models.FileField(null=True, blank=True ,upload_to=assignment_path)
    answer_file = models.FileField(null=True, blank=True ,upload_to=assignment_path)
    cs_remark = models.CharField(verbose_name="remark",null=True,blank=True,max_length=100)
    cancellation_reason = models.CharField(null=True, blank=True, max_length=64, choices=main_settings.CANCELLATION_REASONS)
    IP = models.CharField(max_length=32,null=True)
    cookie = models.CharField(max_length=32, null=True)
    #@property
    def refunded(self):
        if (self.paid + self.additional_payment + self.refund + self.refund_additional <= 0):
            return True
        else:
            return False
    refunded.boolean = True
    refunded.short_description = "Refunded"

    def total_paid(self):
        return self.paid + self.additional_payment + self.refund + self.refund_additional

    def full_payment(self):
        if (self.paid + self.additional_payment >= self.price):
            return True
        else:
            return False
    full_payment.boolean = True
    full_payment.short_description = "Paid"
    def color(self):
        if self.due_on is None:
            color_name = '83786F'
        else:
            nw = datetime.now(timezone.utc)
            t_delta = self.due_on - nw
            min = t_delta.days * 24 * 60 + t_delta.seconds // 60
            hr_left = min // 60
            color_name =  '008000'
            if (hr_left < 0.75*self.duration):
                color_name = 'FFC300'
            if (hr_left < 0.5*self.duration):
                color_name = 'FF8633'
            if (hr_left < 0.25*self.duration):
                color_name = 'C70039'
        return format_html('<span style="color: #{};">&#x2b24;</span>',color_name)
    def due_in(self):
        if self.due_on is None:
            return '---'
        nw = datetime.now(timezone.utc)
        t_delta = self.due_on - nw
        min = t_delta.days * 24* 60 +  t_delta.seconds // 60
        if min < 0:
            txt = "-"
            min = -1 * min
        else:
            txt = ""
        hr = min // 60
        min = min % 60
        txt = txt + str(hr) + " hrs, " + str(min) + " min"
        return txt
    def payment_provider(self):
        try:
            payment = Payment.objects.get(invoice=self.public_id)
            if self.additional_payment == 0:
                return payment.provider
            else:
                add_pmt = Payment.objects.get(invoice=str(self.public_id)+'-ADD')
                return payment.provider + ' + ' + add_pmt.provider
        except Payment.DoesNotExist:
            return 'NA'

    def __str__(self):
        return str(self.public_id)

class Payment(models.Model):
    invoice = models.CharField(max_length=32, unique=True)
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    currency = models.CharField(max_length=4, default='USD')
    refund = models.DecimalField(default=0,max_digits=8, decimal_places=2)
    refunded_on = models.DateTimeField(null=True, blank=True)
    provider = models.CharField(max_length=32,default='paypal')
    gid = models.CharField(max_length=64,null=True, blank=True, help_text='gateway_id', db_index=True)
    status = models.IntegerField(default=0)
    details = models.TextField(null=True, blank=True)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)

class PriceCheck(models.Model):
    email = models.EmailField(null=True,verbose_name="Email")
    subject = models.CharField(max_length=25,choices=main_settings.SUBJECT_CHOICES,default=main_settings.DEFAULT_SUBJECT,verbose_name="Subject")
    grade = models.CharField(max_length=25, choices=main_settings.GRADE_CHOICES, default=main_settings.DEFAULT_GRADE,verbose_name="Grade")
    duration = models.IntegerField(choices=main_settings.DURATION_CHOICES,default=main_settings.DEFAULT_DURATION, verbose_name="Timeline")
    num_questions = models.IntegerField(default=1,verbose_name="Number of Questions")
    short_answer = models.BooleanField(default=False,verbose_name="<b>Only Answer</b> (Without Solution Steps)")
    price = models.DecimalField(max_digits=8, decimal_places=2)
    currency = models.CharField(max_length=3, default='USD')
    cookie = models.CharField(max_length=32,null=True)
    created_date = models.DateTimeField(auto_now_add=True,editable=False)
    def calculate_price(self):
        price_t = main_settings.BASE_PRICE
        if self.short_answer == True:
            price_t *= main_settings.SHORT_ANSWER_FACTOR
        if self.num_questions >= 5:
            price_t *= main_settings.BULK_FACTOR
        price_t *= main_settings.CURRENCIES[self.currency]['value']
        price_t *= main_settings.SUBJECTS[self.subject]['weight']
        price_t *= main_settings.DURATIONS[self.duration]['weight']
        price_t = int(price_t * main_settings.GRADES[self.grade]['weight'])
        price_t *= self.num_questions
        self.price = int(price_t)
        return int(self.price)

class Invoice(models.Model):
    pub_id = models.CharField(max_length=32, unique=True)
    tutor = models.ForeignKey(Tutor, on_delete=models.PROTECT)
    total = models.DecimalField(default=0, max_digits=9,decimal_places=2,)
    year_month = models.PositiveIntegerField()
    note = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return str(self.year_month) + "-" + self.tutor.name.replace(" ","_")
    def calculate_total(self):
        assigns = Assignment.objects.filter(assigned_to=self.tutor)

class Review(models.Model):
    assign = models.ForeignKey(Assignment, on_delete=models.PROTECT, unique=True)
    rating = models.IntegerField(default=0)
    review_txt = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)

# Dirty way of loading signal
import main.utils.paypal_ipn


