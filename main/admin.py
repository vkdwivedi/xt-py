from django.contrib import admin
from django import forms


# Register your models here.
from .models import Assignment,Tutor

from django.db import models
from django.utils.safestring import mark_safe




admin.site.site_header = 'XpressTutor.com Admin'

class AssignmentAdmin(admin.ModelAdmin) :
    model = Assignment
    list_display = ['public_id','student','subject','num_questions','duration','assigned_to','due_on']
    list_filter = ('status' , 'assigned_to')
    search_fields = ('public_id',)

class DashboardManager(models.Manager):
    def get_queryset(self):
        return super(DashboardManager,self).get_queryset().filter(status__in=['PAID','ADD_PAY','WITH_TUTOR'])

class Dashboard(Assignment):
    objects = DashboardManager()
    class Meta:
        proxy = True

class DashboardAdmin(admin.ModelAdmin):
    def get_form(self, request, obj=None, **kwargs):
        kwargs['widgets'] = {'add_payment_note': forms.Textarea(attrs={'rows': 3})}
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['assigned_to'].queryset = Tutor.objects.filter(is_active=True).order_by('name')
        return form
    list_display = ['public_id','color', 'student', 'subject', 'num_questions', 'duration', 'assigned_to','price', 'full_payment','due_in','cs_remark']
    fieldsets = (
        (None,{'fields':('price','total_paid','assigned_to', 'tutor_payment', 'due_on', 'status','cs_remark','upload_answers')}),
        ('Cancel Assignment:',{'fields': ('cancellation_reason','cancel',)}),
        ('Request Additional Payment:',{'fields': ('add_payment_reason','add_payment_note','add_payment',)}),
    )
    readonly_fields = ['cancel','add_payment','total_paid','upload_answers']
    search_fields = ('public_id',)
    def cancel(self, obj):
        if obj.cancellation_reason is None:
            return 'Choose a reason and save before cancelling'
        url = '/assignment/cancel/' + str(obj.public_id)
        js = '''function sendCanLink(){
                    django.jQuery.get("''' + url + '''",function (data){
                        window.scrollTo(0,0);
                        //location.reload();
                        window.location.replace('/admin/main/dashboard/');
                    });
                }'''
        return mark_safe('<script>' + js + '</script><a class="button" onclick="sendCanLink();" style="background:red">&nbsp; Cancel Assignment &nbsp;</a>')
    def add_payment(self, obj):
        if obj.additional_payment > 0:
            return 'Additional payment is made'
        if obj.add_payment_reason is None:
            return 'Choose a reason and save before requesting payment'
        if obj.price <= obj.paid:
            return 'Set a total price and save before requesting payment'
        url = '/assignment/add_payment/' + str(obj.public_id)
        js = '''function sendAddPayLink(){
                    django.jQuery.get("''' + url + '''",function (data){
                        window.scrollTo(0,0);
                        location.reload();
                    });
                }'''
        return mark_safe('<script>' + js + '</script><a class="button" onclick="sendAddPayLink();" style="background:green">&nbsp; Request Payment &nbsp;</a>')
    def upload_answers(self, obj):
        url = '/assignment/upload_answers/' + str(obj.public_id)
        return mark_safe('<a href="'+url+'">Upload Answers</a>')


class ArchiveFilter(admin.SimpleListFilter):
    title = 'all_assignments'
    parameter_name = 'status'
    def lookups(self, request, model_admin):
        return (
            ('com', 'completed'),
            ('can', 'cancelled'),
        )
    def queryset(self, request,queryset):
        if self.value() == 'com':
            return queryset.filter(status__in=['COMPLETED'])
        if self.value() == 'can':
            return queryset.filter(status__in=['CANCELLED'])
        return queryset.filter(status__in=['COMPLETED','CANCELLED'])

class Archive(Assignment):
    class Meta:
        proxy = True

class ArchiveAdmin(admin.ModelAdmin):
    list_display = ['public_id', 'total_paid','payment_provider' ,'student', 'subject', 'num_questions', 'duration', 'refunded' , 'status','cs_remark']
    fields = ['assigned_to', 'price','paid_history','payment_provider', 'due_on', 'status','cs_remark','cancellation_reason','refund_now']
    list_filter = (ArchiveFilter, 'assigned_to')
    readonly_fields = ['refund_now','paid_history','payment_provider']
    search_fields = ('public_id',)
    def paid_history(self,obj):
        return '$' + str(obj.paid + obj.additional_payment) + ' ($' +  str(obj.paid) + ' + $' + str(obj.additional_payment) + ')'
    def refund_now(self, obj):
        if obj.price != 0:
            return 'Set the price to 0 and save before refund'
        if obj.refund < 0:
            return 'The assignment is refunded'
        #if obj.refunded:
            #return 'Refund Done'
        url = '/assignment/refund_assign/' + str(obj.public_id)
        js = '''function sendRefLink(){
                    django.jQuery.get("''' + url + '''",function (data){
                        window.scrollTo(0,0);
                        //location.reload();
                        window.location.replace('/admin/main/archive/?status=can');
                    });
                }'''
        return mark_safe('<script>' + js + '</script><a class="button" onclick="sendRefLink();" style="background:red">&nbsp; Refund Assignment &nbsp;</a>')


class TutorPaymentManager(models.Manager):
    def get_queryset(self):
        return super(TutorPaymentManager,self).get_queryset().filter(status__in=[30])

class TutorPayment(Assignment):
    objects = TutorPaymentManager()
    class Meta:
        proxy = True

class TutorPaymentAdmin(admin.ModelAdmin):
    list_display = ['public_id','assigned_to','price', 'subject', 'num_questions', 'duration', 'status','tutor_payment',]
    fields = ['assigned_to', 'price', 'due_on', 'status']
    list_filter = ('assigned_to',)
    search_fields = ('year_month',)

    #def queryset(self, request):
     #   return self.model.objects.filter(user=request.user)

class TutorAdmin(admin.ModelAdmin):
    list_display = ['name','email','is_active',]
    raw_id_fields = ['user',]
    list_filter = ('is_active',)
    search_fields = ('name',)

admin.site.register(Assignment,AssignmentAdmin)
admin.site.register(Dashboard,DashboardAdmin)
admin.site.register(Archive,ArchiveAdmin)
admin.site.register(TutorPayment,TutorPaymentAdmin)
admin.site.register(Tutor, TutorAdmin)