import string, random
from main.settings import *
# Get choices tuples from a dictionary object for Django model
def get_choices(dict_object):
    choices = ()
    for key,value in dict_object.items():
        temp = (key,value['name'])
        choices += temp
    return choices

def token_alphanum(size):
    chars = string.ascii_lowercase + string.digits
    return ''.join(random.choice(chars) for x in range(size))