from paypal.standard.models import ST_PP_COMPLETED, ST_PP_REFUNDED
from paypal.standard.ipn.signals import valid_ipn_received
from main.models import Assignment
from datetime import datetime, timezone
from django.conf import settings
from django.template.loader import render_to_string
import decimal
from main.views.assignment import fulfill as assign_fulfill, refund as assign_refund
from main.models import Payment

import main.settings as main_settings
from django.core import mail

def ipn_received(sender, **kwargs):
    site_settings = settings.SITE_SETTINGS
    ipn_obj = sender
    print('IPN Signal received')
    assignment_id = ipn_obj.invoice

    # Handle Refund from Paypal
    if ipn_obj.payment_status == ST_PP_REFUNDED:
        amount = ipn_obj.mc_gross
        gid = ipn_obj.parent_txn_id
        payment = Payment.objects.get(gid=gid)
        # Refund only once even if the webhook is called more than once
        if payment.refund == 0:
            payment.refund += decimal.Decimal(amount)
            payment.refunded_on = datetime.now(tz=timezone.utc)
            payment.save()
            assign_refund(assignment_id=assignment_id,amount=-1*ipn_obj.mc_gross)
        return

    if ipn_obj.payment_status == ST_PP_COMPLETED:
        if ipn_obj.payment_status != "Completed":
            return
        txn_details =  str(ipn_obj.posted_data_dict)
        assign_fulfill(assignment_id,amount=ipn_obj.mc_gross, user_name=ipn_obj.first_name, txn_details=txn_details)
        payment = Payment.objects.create(invoice=assignment_id, amount=ipn_obj.mc_gross, gid=ipn_obj.txn_id, provider='paypal', details=txn_details)
    else:
        return

valid_ipn_received.connect(ipn_received)