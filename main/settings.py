
BASE_PRICE = 2.5
SHORT_ANSWER_FACTOR = 0.75
BULK_FACTOR = 0.9
CURRENCIES = {'USD':{'name':'US Dollar','value':1,'symbol':'$'},
              'GBP':{'name':'British Pound','value':0.77,'symbol':'£'},
              'EUR':{'name':'Euro','value':0.87,'symbol':'€'},
              'AUD':{'name':'Australian Dollar','value':1.33,'symbol':'$'},
              'CAD':{'name':'Canadian Dollar','value':1.32,'symbol':'$'}}
SUBJECTS = {
                'math':{'name':'Mathematics','weight':1},
                'stat':{'name':'Statistics','weight':1},
                'phy':{'name':'Physics','weight':1},
                'chem':{'name':'Chemistry','weight':1},
                'eco':{'name':'Economics','weight':1.5},
                'engg':{'name':'Engineering','weight':1.5},
                'mgmt':{'name':'Management & Finance','weight':1.5},
                'prog':{'name':'Programming & Databases','weight':1.5}
            }
DEFAULT_SUBJECT = 'stat'

GRADES = {
            'school':{'name':'High School','weight':1.5},
            'college':{'name':'College','weight':3},
            'research':{'name':'Research/PhD','weight':9}
        }
DEFAULT_GRADE = 'college'

DURATIONS = {
                6:{'name':'6 Hours','weight':2.5},
                12:{'name':'12 Hours','weight':2},
                24:{'name':'24 Hours','weight':1.65},
                72:{'name':'72 Hours','weight':1}
            }
DEFAULT_DURATION = 24
ASSIGNMENT_STATUSES_OLD = ((0,'Created'),
                   (5,'Paid'),
                   (10,'Submitted'),
                   (15,'$ Requested'),
                    (16,'$ Received'),
                    (18,'Cancelled'),
                   (20,'Sent to Tutor'),
                   (25,'Solution Given'),
                   (30,'Completed'))

ASSIGNMENT_STATUSES = (('NEW','Created'),
                   ('PAID','Paid'),
                   ('ADD_PAY','$ Requested'),
                   ('CANCELLED','Cancelled'),
                   ('WITH_TUTOR','Sent to Tutor'),
                   ('COMPLETED','Completed'))
SUBJECT_CHOICES = list()
for subject in SUBJECTS:
    sub_field = (subject,SUBJECTS[subject]['name'])
    SUBJECT_CHOICES.append(sub_field)

GRADE_CHOICES = list()
for grade in GRADES:
    grade_field = (grade,GRADES[grade]['name'])
    GRADE_CHOICES.append(grade_field)

DURATION_CHOICES = list()
for duration in DURATIONS:
    duration_field = (duration,DURATIONS[duration]['name'])
    DURATION_CHOICES.append(duration_field)

ASSIGNMENT_DIRECTORY = "assignments"

CANCELLATION_REASONS = (('TUT_NOT_AVA', 'Tutor Not Available'),
                        ('ADD_PAY_DEC', 'Not Ready to Pay'),
                        ('NO_RESP', 'Not Responding'),
                        ('WRONG_SOL', 'Wrong Solution Given'),
                        ('USER_CAN', 'User Cancelled'),
                        ('OTHER', 'Other Reason/ Not Listed'))

ADD_PAY_REASONS = (('SUB_PARTS','Subparts'),
                   ('DIFFICULTY','Higher Difficulty'),
                   ('PARTS_N_DIFF','Both Subparts and Difficulty'),
                   ('DESCRIPTIVE','Long Descriptive Answers'),)

ADD_PAY_REASONS_DES = {
    'SUB_PARTS':'The questions have multiple parts and require more effort.',
    'DIFFICULTY':'The questions have much higher difficulty level and require more effort.',
    'PARTS_N_DIFF':'The questions are of higher difficulty and there are multiple sub parts to the questions.',
    'DESCRIPTIVE':'The answers are very descriptive and will require more effort.',
}

COINBASE_API_KEY = 'ff9ced78-8014-4fae-a124-9600baaea2ef'